# pylint: disable=C0103

import cv2
import numpy as np
import win32api, win32con
import time
import ImageGrab
from threading import Thread, Lock
import Queue
from visual import *
# from matplotlib import pyplot as plt

# Globals
# ------------------

Done = False
x_pad = 10
y_pad = 12
ul = 0
lr = 0
box = (0,0,0,0)
clickQueue = Queue.Queue()
mutex = Lock()

def setInitialCoords():
    global ul
    global lr
    global box
    print "Go to upper left corner and click right control"
    time.sleep(0.5)
    win32api.GetAsyncKeyState(win32con.VK_RCONTROL)
    while win32api.GetAsyncKeyState(win32con.VK_RCONTROL) == 0:
        # print 'Waiting for Enter'
        time.sleep(0.1)
    ul = get_cords()
    print "Go to lower right corner and click right control"
    time.sleep(0.5)
    win32api.GetAsyncKeyState(win32con.VK_RCONTROL)
    while win32api.GetAsyncKeyState(win32con.VK_RCONTROL) == 0:
        # print 'Waiting for Enter'
        time.sleep(0.1)
    lr = get_cords()

    box = (ul[0], ul[1], lr[0], lr[1])
    # print str(box)
    screenGrab(box)

def clicker():
    while not Done:
        while not clickQueue.empty() and not Done:
            pt = clickQueue.get()
            mousePos((pt[0] + ul[0], pt[1] + ul[1]))
            leftClick()
            print pt
            print clickQueue.qsize()

def work():
    # print box
    global mutex
    while not Done:
        mutex.acquire()
        img_rgb = cv2.imread('ss.PNG')
        mutex.release()
        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
        template = cv2.imread('zerg.PNG', 0)
        w, h = template.shape[::-1]

        res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
        threshold = 0.5
        loc = np.where(res >= threshold)
        global clickQueue
        for pt in zip(*loc[::-1]):
            clickQueue.put(pt)
            # time.sleep(0.1)
            break
        # cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0, 0, 255), 2)
        # cv2.imwrite('res.png', img_rgb)

def screenUpdater():
    while not Done:
        screenGrab(box)

def screenGrab(box):
    # box = (157,346,796,825)
    global mutex
    im = ImageGrab.grab(box)
    mutex.acquire()
    im.save('ss.png', 'PNG')
    mutex.release()

def triggerDone():
    win32api.GetAsyncKeyState(win32con.VK_LCONTROL)
    while win32api.GetAsyncKeyState(win32con.VK_LCONTROL) == 0:
        time.sleep(0.5)
    global Done
    Done = True

def leftClick():
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
    time.sleep(.01)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)
    # print 'Click.'        #completely optional. But nice for debugging purposes.

def mousePos(cord):
    win32api.SetCursorPos((x_pad + cord[0], y_pad + cord[1]))

def get_cords():
    x,y = win32api.GetCursorPos()
    x = x - x_pad
    y = y - y_pad
    print x, y
    return (x, y)

def main():
   setInitialCoords()

   t1 = Thread(target=screenUpdater)
   t2 = Thread(target=work)
   t3 = Thread(target=clicker)
   t4 = Thread(target=triggerDone)

   t1.start()
   t2.start()
   t3.start()
   t4.start()
   pass

if __name__ == '__main__':
    main()
